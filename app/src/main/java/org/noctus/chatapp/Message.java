package org.noctus.chatapp;

public class Message {
    public String text;
    public String name;
    public String thumbnail;
    public long time;
}
